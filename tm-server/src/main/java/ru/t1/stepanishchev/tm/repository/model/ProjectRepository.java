package ru.t1.stepanishchev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    @NotNull
    List<Project> findByUserId(@NotNull String userId);

    @Nullable
    Project findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.user = :userId ORDER BY :sortType")
    List<Project> findAllByUserIdAndSort(@NotNull @Param("userId") String user, @NotNull @Param("sortType") String sortType);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    long countByUserId(@NotNull final String userId);

}