package ru.t1.stepanishchev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.ProjectClearRequest;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    private final static String NAME = "project-clear";

    @NotNull
    private final static String DESCRIPTION = "Remove all projects.";

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[PROJECTS CLEAR]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        projectEndpoint.clearProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}